---
title: IFB NNCR Cluster documentation
---

# Welcome to the IFB NNCR Cluster documentation

The Institut Français de Bioinformatique (IFB) National Network of Compute Resources (NNCR) Cluster designates the "federation" of the IFB's HPC/Cluster computing infrastructure.

<a href="https://www.france-bioinformatique.fr/">
<img src="./imgs/IFB-HAUT-COULEUR-PETIT.png" alt="IFB logo" style="width:500px; margin-left: 60px;"/>
</a>


The IFB NNCR Cluster goal is to share practices, developments, expertises across the different HPC providers.

This documentation aims to give the first hints to:

 - use Ansible on an existing NNCR Cluster node
 - adopt some parts of the IFB Core Cluster developments
 - connect a regional Cluster to IFB Core Cluster CI system

