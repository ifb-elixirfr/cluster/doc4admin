# Aide aux calculs des indicateurs annuelles

## General scripts

A lancer une fois pour aider aux calculs de plusieurs indicateurs.

### `resource_usage.sh`

```bash
#!/bin/bash

# Format: 2019-01-01
START=$1
END=$2

projects=`sacctmgr -n -P list association format=account WithDeleted | grep -v taskforce | grep -v root | sort -u `
echo -e "Project,CPUTime"
for project in $projects; do
 cputime=`sreport -n -P -t hour Cluster AccountUtilizationByUser start=$START end=$END format=Login,Used accounts=$project | sed '/jseiler\|glecorguille\|gseith\|jhaessig\|ncharriere\|dbenaben\|sihara\|tfbulle\|fgerbes/d' | sed 1d - | awk -F '|' '{ hcpu+=$2 } END { print hcpu} '`
 if [ -z $cputime ]; then
   cputime="0"
 fi
 echo -e "${project},${cputime}"
done
```

Les membres de la taskforce ont été enlevé suite à des réservations pour maintenance. SLURM va en effet compter 100% d'usage de ces réservations.

```bash
export START=2020-01-01 END=2020-12-31
bash resource_usage.sh $START $END > resource_usage-${START}-${END}.tab
```

___

## Retombées communauté scientifique

### Nombre total d'usagers actifs

#### SLURM

```bash
export START=2020-01-01 END=2020-12-31;
sreport -n -P -t hour Cluster AccountUtilizationByUser start=$START end=$END format=login,used | awk -F "|" '$1 != "" { tab[$1]+=$2 } END { for (i in tab) if (tab[i] >= 1) print i"\t"tab[i] }' | sort -k2 -nr > sreport-AccountUtilizationByUser${START}-${END}-refactored.tab
slurm_active_user=$(wc -l < sreport-AccountUtilizationByUser${START}-${END}-refactored.tab)
```


#### usegalaxy.fr 

De `galaxy@clust-usegalaxy-ubuntu` 

```bash
export START=2020-01-01 END=2020-12-31;
usegalaxy_active_user=$( psql -U galaxy-admin -h 192.168.16.213 galaxy -t -c "select count(id) from galaxy_user where id in (select distinct user_id from history where id in (select distinct history_id from job where create_time >= '$START' AND create_time <= '$END'))" );
```


#### Total 

```bash
echo $(( $slurm_active_user + $usegalaxy_active_user ))
```



### Nombre de nouveaux comptes utilisateur ouverts

#### SLURM

```bash
declare -i max_old
declare -i cur_old
max_old=33
cur_old=0
while [ $max_old -gt $cur_old ]
do
    range_max=$(date +"%Y%m" -d "-$cur_old months")
    cur_old+=1
    range_min=$(date +"%Y%m" -d "-$cur_old months")
    filter='(&(createTimestamp>='$range_min'01000000Z)(createTimestamp<='$range_max'01000000Z))'
    #echo "$filter"
    nb_user=$(ldapsearch -h openldap.ifb.local -xLLL -b ou=people,dc=ifb,dc=local "$filter" | grep 'uid:' | wc -l)
    echo "Between $range_min and $range_max there are $nb_user new users"
done
```



#### usegalaxy.fr 

```bash
usegalaxy_new_user=$(gxadmin tsvquery monthly-users-registered 2020 | awk '{ TOTAL+=$2 } END { print TOTAL }')
```


#### Total 

```bash
echo $(( $slurm_new_user + $usegalaxy_new_user ))
```


### Nombre de nouveaux espaces-projets partagés ouverts


```bash
declare -i max_old
declare -i cur_old
max_old=33
cur_old=0
while [ $max_old -gt $cur_old ]
do
    range_max=$(date +"%Y%m" -d "-$cur_old months")
    cur_old+=1
    range_min=$(date +"%Y%m" -d "-$cur_old months")
    filter='(&(createTimestamp>='$range_min'01000000Z)(createTimestamp<='$range_max'01000000Z))'
    #echo "$filter"
    nb_user=$(ldapsearch -h openldap.ifb.local -xLLL -b ou=projects,ou=groups,dc=ifb,dc=local "$filter" | grep 'cn:' | wc -l)
    echo "Between $range_min and $range_max there are $nb_user new projects"
done
```

### Nombre total d'espaces-projets actifs

```bash
awk -F "," '$2 > 0 { count++ } END { print count }' resource_usage-2020-01-01-2020-12-31.tab
```

### Nombre d'utilisateurs uniques pour l'ensemble des interfaces web de la plateforme

#### usegalaxy

CF -> Nombre total d'usagers actifs > usegalaxy


#### Jupyter

```bash
export START=2021-01-01 END=2021-12-31;
jupyter_active_user=$(sacct --allusers -S $START -E $END --name=jupyter --format=User -X -s CA,CD,TO -n | sort | uniq | wc -l)
```

#### Total

```bash
echo $(( $usegalaxy_active_user + $jupyter_active_user ))
```

## Formations









## Projets et Infra









## Infrastructure physique


### Nombre de coeurs CPUs
```bash
sinfo -Ne  --format "%.14N %.7z %.7m " -S w,-T,N | sort | uniq | awk '{ split($2, CPU, ":"); CPUTOTAL+=(CPU[1]*CPU[2]*CPU[3]); MEMTOTAL+=$3 } END { print "CPU TOTAL: " CPUTOTAL " - hCPU/year:" (CPUTOTAL*365*24) " - MEM TOTAL: " MEMTOTAL}'
```





## Taux d'utilisation

### Temps d'occupation machines pour hébergement

/!\ 2 techniques et 2 résultats très différents

```bash
export START=2020-01-01 END=2020-12-31;
hCPU=$(cat resource_usage-${START}-${END}.tab  | sed "1d" | awk -F ',' '{ hcpu+=$2 } END { print hcpu} ')
```

Ou 

```bash
export START=2020-01-01 END=2020-12-31;
sreport -t HourPer cluster Utilisation Start=$START End=$END
```

### Moyenne / Ecart-type par usager actif

```bash
export START=2020-01-01 END=2020-12-31;
sreport -n -P -t hour Cluster AccountUtilizationByUser start=$START end=$END format=login,used | awk -F "|" '$1 != "" { tab[$1]+=$2 } END { for (i in tab) if (tab[i] >= 1) print i"\t"tab[i] }' | sort -k2 -nr > sreport-AccountUtilizationByUser${START}-${END}-refactored.tab
awk '{ sum += $2; sumi[NR] = $2 } END { mean=sum/NR; for (i in sumi) { stdevd += ((sumi[i] - mean)^2) } stdev = sqrt(stdevd / (NR-1)); printf "mean = %d\tstdev = %d",mean,stdev }' sreport-AccountUtilizationByUser${START}-${END}-refactored.tab
```

### Taux global d’utilisation par année, sur le temps d’ouverture aux utilisateurs

```bash
hCPU_year=$(sinfo -Ne  --format "%.14N %.7z" -S w,-T,N | sort | uniq | awk '{ split($2, CPU, ":"); CPUTOTAL+=(CPU[1]*CPU[2]*CPU[3]);} END { print (CPUTOTAL*365*24)}')
```

```bash
echo $(( 100 * $hCPU / $hCPU_year ))
```

