# The IFB r-packages GitLab repository

GitLab repository: [https://gitlab.com/ifb-elixirfr/cluster/r-packages](https://gitlab.com/ifb-elixirfr/cluster/r-packages)

The repository allows to manage the deployment of R packages for the IFB clusters federation.


## How it works

Thanks to GitLab CI/CD pipeline ([.gitlab-ci.yml](https://gitlab.com/ifb-elixirfr/cluster/r-packages/-/blob/master/.gitlab-ci.yml)), the R packages defined in the [vars](https://gitlab.com/ifb-elixirfr/cluster/r-packages/-/tree/master/vars) directory on the repository can be automatically installed on the IFB clusters federation.

R packages list depends on R version.

Packages are installed with an ansible playbook (and ansible roles associated).

GitLab CI/CD launch a job for each Cluster (IFB, ABiMS, MCIA, etc) and for several R version/mode.


## Requirements

#### R prevously installed

R must be previously installed with as example [tools](https://gitlab.com/ifb-elixirfr/cluster/tools).


#### SSH access or gitlab-runner agent

You need a SSH access (with key authentication) to the cluster or a GitLab runner agent running on your infrastructure.


##  Add a new cluster

Integrate the GitLab CI/CD pipeline.

MergeRequest example: [https://gitlab.com/ifb-elixirfr/cluster/r-packages/-/merge_requests/91https://gitlab.com/ifb-elixirfr/cluster/r-packages/-/merge_requests/91](https://gitlab.com/ifb-elixirfr/cluster/r-packages/-/merge_requests/91)


### Add host

Host which execute playbook (and install R packages)

Example with MCIA: `production/mcia-cluster/hosts`
```
[r-env]
curta.mcia.fr	ansible_user=deployuser
```

### Add variables

Define the variables suitable for your site

Example with MCIA: `production/mcia-cluster/group_vars/r-env`
```yaml
---

conda_home: "/gpfs/softs/contrib/ifb/miniconda3"
conda_env_home: "{{ conda_home }}/envs"

r_packages_owner: "deployuser"
r_packages_group: "inra"

r_scripts_install_path: "/gpfs/softs/contrib/ifb/Rscripts"
r_scripts_install_owner: "{{ r_packages_owner  }}"
r_scripts_install_group: "{{ r_packages_group }}"
```


### Integrate GitLab pipeline (GitLab CI/CD)

Integrate GitLab deployment pipeline allows to deploy the new added packages.

1. **Add new jobs to the pipeline**  
   Add a job for each stage and R version/mode

2. **Authorize SSH key for GitLab CI/CD**  
   If you use SSH access (instead of a gitlab-runner), a SSH private key must be deployed. Ask to an admin.


Example with MCIA Cluster:  
```yaml
MCIA Ansible Check (Prod):
  extends:
    - .prod
    - .idris
  stage: Check
  script: "ansible-playbook -vvv -i production/mcia-cluster --syntax-check playbook_*.yml"
  needs: ["IFB Ansible Requirements (Prod)", "IFB Gathering Facts (Prod)"]

MCIA r-4.0.3 Packages (Prod):
  extends:
    - .prod
    - .idris
  stage: Install
  script: "ansible-playbook -i production/mcia-cluster playbook_r-install-package.yml -e 'r_env=r-4.0.3'"
  needs: ["MCIA Ansible Check (Prod)"]
```


##  Install all R packages

**Clone the repository**
```
git clone git@gitlab.com:ifb-elixirfr/cluster/r-packages.git
```

**Ansible requirements**
```
# ansible.cfg
export GIT_SSL_NO_VERIFY=1
export ANSIBLE_REMOTE_USER=root
export ANSIBLE_PIPELINING=True
export ANSIBLE_NOCOWS=True
export ANSIBLE_ROLES_PATH=downloaded_roles
export ANSIBLE_CACHE_PLUGIN=jsonfile
export ANSIBLE_CACHE_PLUGIN_CONNECTION=cached_facts

ansible-galaxy install -vvv --force --force-with-deps -r requirements.yml -p ./downloaded_roles
```

**Execute the playbook**

On your inventory and with the R version/mode:
```
ansible-playbook -i production/mcia-cluster playbook_r-install-package.yml -e 'r_env=r-4.0.2' -e 'r_mode=full'
```

Note: if you execute the playbook locally (not with a SSH connection) you can specify `-c local`.

<details class="example">
<summary>Installation script example (for all R version)</summary>

```
#!/bin/bash

#SBATCH --job-name=IFB-r-packages
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --time=1-00:00:00
#SBATCH --chdir=/gpfs/softs/contrib/ifb/
#SBATCH --output=/gpfs/softs/contrib/ifb/logs/slurm_r-packages_%j.log

#
# USAGE:
# sbatch build_r-packages.sh
# sbatch --export=RVERSION=r-4.0.2 build_r-packages.sh
#

echo "#############################" 
echo "User:" $USER
echo "Date:" $(date)
echo "Host:" $(hostname)
echo "Directory:" $(pwd)
echo "SLURM_JOBID:" $SLURM_JOBID
echo "#############################" 

echo "Load environement" 

export GIT_SSL_NO_VERIFY=1
export ANSIBLE_REMOTE_USER=dbenab100p
export ANSIBLE_PIPELINING=True
export ANSIBLE_NOCOWS=True
export ANSIBLE_ROLES_PATH=downloaded_roles
export ANSIBLE_CACHE_PLUGIN=jsonfile
export ANSIBLE_CACHE_PLUGIN_CONNECTION=cached_facts


RENVS=$(ls /gpfs/softs/contrib/ifb/miniconda3/envs/ | grep -E '^r-[0-9\.]+')
echo "R environments:"
echo "$RENVS"

# If RVERSION env variable is set, use it
if [ -n "$RVERSION" ]
then
   RENVS=$RVERSION
fi

for RVERSION in $RENVS
do

  echo "##########################################################"
  echo "r-env=$RVERSION"

  echo "##########################################################"
  echo -n "PRE - Number of libraries: "
  ls /gpfs/softs/contrib/ifb/miniconda3/envs/$RVERSION/lib*/R/library | wc -l
  echo "Start: $(date)"
  echo "##########################################################"

  # GO to IFB r-packages repository
  cd /gpfs/softs/contrib/ifb/r-packages

  echo "Install ! (localement)"
  ansible-playbook -c local -i production/mcia-cluster playbook_r-install-package.yml --diff  -e "r_env=$RVERSION" -e 'r_mode=full'


  echo "##########################################################"
  echo -n "POST - Number of libraries: "
  ls /gpfs/softs/contrib/ifb/miniconda3/envs/$RVERSION/lib*/R/library | wc -l
  echo "End: $(date)"
  echo "##########################################################"

done

echo "#############################" 
echo "Finished: $(date)"

```
</details>

Job take around **2 hours per R environment** (first install) and 10 minutes without installation (just check, R packages already installed)
