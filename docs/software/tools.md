# The IFB Tools GitLab repository


The repository allows to manage the deployment of tools for the IFB clusters federation:
[https://gitlab.com/ifb-elixirfr/cluster/tools](https://gitlab.com/ifb-elixirfr/cluster/tools)


## Add a software

Refer to the [README](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/README.md).


## Add a cluster

Thanks to GitLab CI/CD pipeline ([.gitlab-ci.yml](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/.gitlab-ci.yml)), the tools defined in the repository are automatically deployed on all the IFB clusters federation.

You only need an SSH access to the cluster and [Environment Modules](https://modules.readthedocs.io/) to show and manipulate softwares.


### How it works

GitLab CI/CD launch a job for each Cluster (IFB, ABiMS, IGBMC, CCUS, BiRD, etc) and for each type of deployment (Conda, Singularity).

New tool integrated to the repository (MR accepted) &rarr; launch pipeline.

Pipeline: execute `ci/check_changes.sh` (build list of tools to deploy) and launch a _conda_ or _singularity_ _job_ on each cluster

#### Conda Job

A conda environment is generated for each software (1 environment = 1 tool).

```bash
# conda environments:
base                     /shared/software/ifb/miniconda3
abyss-2.2.1              /shared/software/ifb/miniconda3/envs/abyss-2.2.1
admixture-1.3.0          /shared/software/ifb/miniconda3/envs/admixture-1.3.0
alientrimmer-0.4.2       /shared/software/ifb/miniconda3/envs/alientrimmer-0.4.2
anvio-6.2                /shared/software/ifb/miniconda3/envs/anvio-6.2
[...]
```

And a modulefile is created pointing on the bin directory for each tools.
Exemple with abyss/2.2.1:
```bash
#%Module1.0################
module-whatis "Origin: Conda packages"
module-whatis "Description: Assembly By Short Sequences - a de novo, parallel, paired-end sequence assembler"
conflict abyss
prepend-path PATH /gpfs/softs/contrib/ifb/miniconda3/bin/
prepend-path PATH /gpfs/softs/contrib/ifb/miniconda3/envs/abyss-2.2.1/bin
prepend-path LD_LIBRARY_PATH /gpfs/softs/contrib/ifb/miniconda3/envs/abyss-2.2.1/lib
prepend-path CPATH /gpfs/softs/contrib/ifb/miniconda3/envs/abyss-2.2.1/include
```

The creation can be done via SSH (on the remote cluster).

Cluster parameters are defined through environment variables: _REMOTE_MODE_, _CONDA_HOME_, _MODULEFILES_PATH_, _CLUSTER_HOST_, _CLUSTER_SSH_KEY_ and _CLUSTER_USER_.

```bash
# .gitlab-ci.yml
SSH add key
ci/conda/create_env.sh ci/conda-tools.list
```
```bash
# ci/conda/create_env.sh
ssh user@cluster conda env create ...
create_module_file
```

#### Singularity Job

Singularity images are built on IFB Core infrastructure (inside a docker).

For each softwares (commands) inside the image, a bash wrapper is generated like this (example with `relion` software)
```bash
#! /usr/bin/env bash
singularity exec --nv /gpfs/softs/contrib/ifb/singularity/images/relion-3.0.7.sif relion $@
```
Wrapper files are located in `$SINGULARITY_REPOSITORY_PATH/wrappers/<tool>/<version>/`.

A module file is created pointing on this wrapper directory.
Exemple with relion:
```bash
#%Module1.0################
module-whatis "Origin: Singularity image"
module-whatis "Description: Relion is a stand-alone computer program that employs an empirical Bayesian approach to refinement of (multiple) 3D reconstructions or 2D class averages in electron cryo-microscopy (cryo-EM)"
conflict relion
module load singularity
prepend-path PATH /gpfs/softs/contrib/ifb/singularity/wrappers/relion/3.0.7
```

Each files are generated on directory which might be a SSHFS mount point (on the remote cluster).

Cluster parameters are defined through environment variables: _MODULEFILES_PATH_, _SINGULARITY_REPOSITORY_PATH_, _SOFTWARE_SRC_PATH_, _CLUSTER_HOST_, _CLUSTER_SSH_KEY_ and _CLUSTER_USER_.


```bash
# .gitlab-ci.yml
SSH add key
sshfs user@cluster singularity_repository_path
sshfs user@cluster src_repository_path
sshfs user@cluster modulefiles_repository_path
ci/singularity/build_images.sh ci/singularity-tools.list
```
```bash
# ci/singularity/build_images.sh
singularity build ...
Creating wrappers
Creating modulefile
```

<details class="info" open>
<summary>Software under licence or commercial</summary>
We can manage software that cannot be downloaded directly from the web because it requires the acceptance of a license (academic use ...) or because it is not free.<br/>
For that, the repo allows to use sources located on the filesystem of the infrastructure. If these sources are not available on a platform, the build will just be skipped</details>

### Requirements

#### SSH access

You only need a SSH access (with key authentication).

You can create a dedicated key.
```bash
# Generate
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_ifb-tools_user_cluster -C ifb-tools_user_cluster
# Authorize
sh-copy-id -i  ~/.ssh/id_ed25519_ifb-tools_user_cluster.pub -f user@cluster
```

#### Storage / Directory

Some storage space, around **500GB** should be sufficient (2020-02-22: */shared/ifbstor1/software* = 210GB)


A directory where softwares will be installed. You can choose a dedicated directory where all IFB tools will be managed (like `/shared/software/ifb`).

<details class="info" open>
<summary>Hard-Links</summary>
Conda uses hard-links. Standard file system, NFS, GPFS, Lustre can manage this hard-links but not some specialized FS.
</details>


#### Environment Modules

[Environment Modules](https://modules.readthedocs.io/) are used as a frontend to show and load softwares. The softwares are installed by other tools like Conda or Singularity.

We encourage its usage inside the IFB NNCR Cluster federation to have an uniform user experience.

Cluster administrator can add modulefiles path (like */shared/software/ifb/modulefiles*) to the environement, so users can access directly the softwares through `module` commands.


#### Conda (optionnal)

If you want to use tools deployed with [Conda](https://conda.io/), you can install Miniconda: https://docs.conda.io/en/latest/miniconda.html

You do not need any special rights or permissions. Just specify the directory to use (like `/shared/software/ifb/miniconda3`)

<details class="example">
<summary>Installation example</summary>

* Download:
```
$ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
$ sha256sum Miniconda3-latest-Linux-x86_64.sh
```

* Install and specify location (like `/shared/software/ifb/miniconda3`):
```
$ bash Miniconda3-latest-Linux-x86_64.sh

Miniconda3 will now be installed into this location:
/home/user/miniconda3

  - Press ENTER to confirm the location
  - Press CTRL-C to abort the installation
  - Or specify a different location below

[/home/user/miniconda3] >>> /shared/software/ifb/miniconda3


You have chosen to not have conda modify your shell scripts at all.
To activate conda's base environment in your current shell session:

eval "$(/shared/software/ifb/miniconda3/bin/conda shell.YOUR_SHELL_NAME hook)" 

To install conda's shell functions for easier access, first activate, then:

conda init
```
</details>


You can also define a conda environment to manage this softwares like this:
```
export PYTHON_VERSION='3.8'
export CONDA_VERSION='4.9.0'
export MAMBA_VERSION='0.6.4'
conda env create -n tools -c conda-forge python=${PYTHON_VERSION} conda=${CONDA_VERSION} mamba=${MAMBA_VERSION} j2cli pyyaml
```
Or using an environement file
<details class="example">
<summary>Environment file example</summary>

```
name: tools
channels:
  - conda-forge
dependencies:
  - python=3.8
  - conda=4.9.0
  - mamba=0.6.4
  - j2cli=0.3.10
  - pip:
    - pyyaml==5.4.1
```
</details>



#### Singularity (optionnal)

If you want to use tools deployed with [Singularity](https://sylabs.io/docs/), you need to have singularity installed.

Singularity must be accessible with [Environment Modules](https://modules.readthedocs.io/), like: `module load singularity`.

If it not the case, there are some solutions. Discuss with us or request some change on the repository.



###  Add a new cluster (GitLab CI/CD)

Integrate GitLab deployment pipeline allows to deploy the new added tools.

1. **Add new jobs to the pipeline**  
   One job for each deployment: conda and/or singularity.  
   Connection parameters and paths are variables to defined.

2. **Add a protected variable inside GitLab CI/CD interface**  
   This variable is the SSH key authorzed on the remote user@cluster (like BiRD_CLUSTER_SSH_KEY).  
   https://gitlab.com/ifb-elixirfr/cluster/tools/-/settings/ci_cd


Example with BiRD Cluster: https://gitlab.com/ifb-elixirfr/cluster/tools/-/merge_requests/397

```yaml
BiRD Conda:
  extends: .deploy_conda_tools
  stage: Production
  variables:
    REMOTE_MODE: "true"
    CONDA_HOME: /LAB-DATA/BiRD/apps/ifb-nncr/miniconda
    MODULEFILES_PATH: /LAB-DATA/BiRD/apps/ifb-nncr/modulesfiles
    CLUSTER_HOST: bird2login.univ-nantes.fr
    CLUSTER_SSH_KEY: $BiRD_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb-tools
```

```yaml
BiRD Singularity:
  extends: .deploy_singularity_tools
  stage: Production
  variables:
    SINGULARITY_REPOSITORY_PATH: /LAB-DATA/BiRD/apps/ifb-export
    MODULEFILES_PATH: /LAB-DATA/BiRD/apps/ifb-nncr/modulesfiles
    SOFTWARE_SRC_PATH: /LAB-DATA/BiRD/apps/ifb-nncr/src
    CLUSTER_HOST: bird2login.univ-nantes.fr
    CLUSTER_SSH_KEY: $BiRD_CLUSTER_SSH_KEY
    CLUSTER_USER: ifb-tools
```


###  Install one or all tools manually

Clone the tools repository:
```
git clone https://gitlab.com/ifb-elixirfr/cluster/tools.git
```

Define the environment variables suitable for your site.

```
export MODULEFILES_PATH=/shared/software/ifb/modulefiles
export CONDA_HOME=/shared/software/ifb/miniconda3
export SINGULARITY_REPOSITORY_PATH=/shared/software/ifb/singularity
export SOFTWARE_SRC_PATH=/shared/software/ifb/src
```

And use `utils` scripts: [https://gitlab.com/ifb-elixirfr/cluster/tools/-/tree/master/utils](https://gitlab.com/ifb-elixirfr/cluster/tools/-/tree/master/utils) to install the softwares.

Note: without special permissions, it's not possible to build Singularity images. In this case, you can directly copy images from the IFB Core Cluster node (`/shared/software/singularity/images/`). Be careful, some images/softwares are under license constraints, so don't use these images unless you have the right to do it.

`utils/local_install.sh` to install specific software
```
# Example with adxv (singularity)
utils/local_install.sh adxv 1.9.14
# Example with blast (conda)
# -c <conda_home>
# -m <modulefiles_path>
utils/local_install.sh -c /shared/software/ifb/miniconda3 -m /shared/software/ifb/modulefiles blast 2.9.0
```

Or `utils/local_install_all.sh` to deploy all tools:
```
utils/local_install_all.sh
```

<details class="example">
<summary>Slurm job example</summary>

```
#!/bin/bash

#SBATCH --job-name=IFB-build-all-tools
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --time=1-00:00:00
#SBATCH --chdir=/shared/software/ifb/

echo "Started: $(date)"

echo "Load environement" 

eval "$(/shared/software/ifb/miniconda3/bin/conda shell.bash hook)"
conda activate tools

module load singularity

export MODULEFILES_PATH=/shared/software/ifb/modulefiles
export CONDA_HOME=/shared/software/ifb/miniconda3
export SINGULARITY_REPOSITORY_PATH=/shared/software/ifb/singularity
export SOFTWARE_SRC_PATH=/shared/software/ifb/src

# Go to IFB tools repository
cd /shared/software/ifb/tools/

echo "Build !"
srun bash utils/local_install_all.sh

echo "Finished: $(date)"

```
</details>

Job take around **8 hours** (2021) to install all tools.

You can test module environment integration by adding your modulefiles path.
```
$ export MODULEPATH=/shared/software/ifb/modulefiles:$MODULEPATH
$ module av
```


###  Relaunch the pipeline (build) for one software

1. Go to the "CI/CD pipeline" menu (tools repository): [https://gitlab.com/ifb-elixirfr/cluster/tools/-/pipelines](https://gitlab.com/ifb-elixirfr/cluster/tools/-/pipelines)

2. Button "Run pipeline"

3. Set the variables `SOFTWARE` and `VERSION` to the corresponding software.

Example with `interproscan/5.52-86.0`:

<img src="../imgs/tools_run-pipeline-software.png" alt="Tools: run pipeline" style="width:600px;"/>

